import * as helmet from 'helmet';
import * as morgan from 'morgan';
import { CRON_APP_PORT } from '../config';
import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './app.module';

(async function bootstrap () {
  const app = await NestFactory.create(ApplicationModule, {
    bodyParser: true
  });

  app.use(helmet());
  app.use(morgan('dev'));

  await app.listen(CRON_APP_PORT);

  process.stdout.write(`Cron server is running in port ${CRON_APP_PORT}\n`);
})();
