import { Component, Inject, OnModuleInit } from '@nestjs/common';
import { DB_CONNECTION_TOKEN } from '../main/constants';
import { Connection } from 'typeorm';
import { FeatureConfigValueEntity } from '../main/modules/feature-config/feature-config-value.entity';
import { AxiosStatic } from 'axios';
import { AXIOS_CLIENT_TOKEN } from './constants';
import * as cron from 'cron';
import { CRON_APP_KEY } from '../config';
import * as Holidays from 'date-holidays';
import * as moment from 'moment-timezone';
import { StandUpEntity } from '../main/modules/stand-up/stand-up.entity';

@Component()
export class CronService implements OnModuleInit {
  private readonly timezone = 'Europe/London';
  private readonly holidays = new Holidays('GB');
  private readonly timeoutInMinutes = 30;
  private readonly timeout = 1000 * 60 * this.timeoutInMinutes;

  constructor (
    @Inject(DB_CONNECTION_TOKEN)
    private readonly dbConnection: Connection,
    @Inject(AXIOS_CLIENT_TOKEN)
    private readonly http: AxiosStatic
  ) {}

  public async onModuleInit () {
    this.initializeCronJobs();
    this.handlePendingTimeouts();
  }

  public async initializeCronJobs () {
    (await this.findJobs()).forEach(v => this.scheduleTask(v));
  }

  public async handlePendingTimeouts () {
    (await this.findPendingTimeouts()).forEach(v => {
      const [h, m] = (v.time as string).split(':').map(v => Number(v));

      const fn = () => {
        this.handleStandupTimeout({
          id: v.id,
          hour: h,
          minute: m,
          second: 0
        });
      };

      if ((v.seconds_ago / 60) >= this.timeoutInMinutes) {
        this.log(`Executing pending timeout.`);
        fn();
      } else {
        const time = this.timeout - (v.seconds_ago * 1000);

        this.log(`Starting pending timeout to be executed in ${(time / 1000 / 60).toFixed(1)} minutes`);

        setTimeout(fn, time);
      }
    });
  }

  public scheduleTask ({ time, id }: { time: string, id: number }) {
    const [h, m] = time.split(':').map(v => Number(v));
    const s = 0;

    // tslint:disable-next-line:no-unused-expression
    new cron.CronJob(`${s} ${m} ${h} * * *`, async () => {
      this.log(`Executing tasks for standup with id ${id} at time ${time}`);

      const date = moment.utc({
        hour: h,
        minute: m,
        second: s
      }).tz(this.timezone);

      if ([0, 6].includes(date.day())) {
        this.log(`Skipping task with id ${id} due to weekend`);
        return;
      }

      if (this.holidays.isHoliday(date.toISOString())) {
        this.log(`Skipping task with id ${id} due to holiday`);
        return;
      }

      try {
        await this.http.post('/commands/cron/groupalstandup', {
          token: CRON_APP_KEY,
          id,
          datetime: date.utc().format()
        });
        setTimeout(() => {
          this.handleStandupTimeout({
            id,
            hour: date.utc().hour(),
            minute: date.utc().minute(),
            second: date.utc().second()
          });
        }, this.timeout);
      } catch (e) {
        this.log(`Error while executing task for standup ${id}: ${e.message}`);
      }
    }, undefined, true, 'UTC');

    this.log(`Started cron job for standup with id ${id} to be executed at ${time}`);
  }

  private async handleStandupTimeout (
    payload: { id: number, hour: number, minute: number, second: number }
  ) {
    const notFinishedStandups = await this.findNotFinishedStandups(payload);

    if (!notFinishedStandups.length) {
      return;
    }

    try {
      await this.http.post('/commands/cron/notify-missing-users', {
        token: CRON_APP_KEY,
        feature_config_id: payload.id,
        stand_ups: notFinishedStandups
      });
    } catch (e) {
      this.log('There was an error while notifying missing users to main server: ' + e.message);
    }
  }

  private findNotFinishedStandups (
    { id, hour, minute, second }:
    { id: number, hour: number, minute: number, second: number }
  ) {
    return this.dbConnection
      .createQueryBuilder()
      .select('s.id, s.user_id')
      .from(StandUpEntity, 's')
      .leftJoin('s.conversation', 'c')
      .leftJoin('c.feature_config', 'fc')
      .where('fc.id = :id', { id })
      .andWhere('c.is_finished = false')
      .andWhere('s.was_notified = false')
      .andWhere('HOUR(s.datetime) = :hour', { hour })
      .andWhere('MINUTE(s.datetime) = :minute', { minute })
      .andWhere('SECOND(s.datetime) = :second', { second })
      .andWhere(qb => {
        // Select only the standups from the last executed datetime
        const subquery = qb.subQuery()
          .select('date(max(s2.datetime))')
          .from(StandUpEntity, 's2')
          .leftJoin('s2.conversation', 'c2')
          .leftJoin('c2.feature_config', 'fc2')
          .where('fc2.id = :id', { id })
          .andWhere('HOUR(s2.datetime) = :hour', { hour })
          .andWhere('MINUTE(s2.datetime) = :minute', { minute })
          .andWhere('SECOND(s2.datetime) = :second', { second })
          .getQuery();

        return `date(s.datetime) = ${subquery}`;
      })
      .getRawMany();
  }

  private findJobs () {
    return this.dbConnection
      .createQueryBuilder()
      .select('fcv.configuration_value time, fcv.featureConfigId id')
      .from(FeatureConfigValueEntity, 'fcv')
      .addFrom(FeatureConfigValueEntity, 'fcv2')
      .where('fcv.featureConfigId = fcv2.featureConfigId')
      .andWhere('fcv.configuration_key = "time"')
      .andWhere('fcv2.configuration_key = "is_auto_start"')
      .andWhere('fcv2.configuration_value = true')
      .getRawMany();
  }

  private findPendingTimeouts () {
    return this.dbConnection
      .createQueryBuilder()
      .select('fcv.configuration_value time')
      .addSelect('fcv.featureConfigId id')
      .addSelect('timestampdiff(second, s.datetime, now()) seconds_ago')
      .from(FeatureConfigValueEntity, 'fcv')
      .leftJoin(FeatureConfigValueEntity, 'fcv2', 'fcv.featureConfigId = fcv2.featureConfigId')
      .leftJoin('conversation', 'c', 'fcv.featureConfigId = c.featureConfigId')
      .leftJoin('stand_up', 's', 's.conversationId = c.id')
      .andWhere('fcv.configuration_key = "time"')
      .andWhere('fcv2.configuration_key = "is_auto_start"')
      .andWhere('fcv2.configuration_value = true')
      .andWhere('c.is_finished = false')
      .andWhere('s.was_notified = false')
      .groupBy('fcv.configuration_value')
      .addGroupBy('fcv.featureConfigId')
      .addGroupBy('s.datetime')
      .getRawMany();
  }

  private log (message: string) {
    process.stdout.write(message + '\n');
  }
}
