import { AXIOS_CLIENT_TOKEN } from './constants';
import axios from 'axios';
import { APP_PORT } from '../config';

export const cronProviders = [
  {
    provide: AXIOS_CLIENT_TOKEN,
    useValue: axios.create({
      baseURL: `http://localhost:${APP_PORT}`
    })
  }
];
