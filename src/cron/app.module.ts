import { Module } from '@nestjs/common';
import { DatabaseModule } from '../main/modules/database/database.module';
import { CronService } from './cron.service';
import { cronProviders } from './cron.providers';
import { CronController } from './cron.controller';

@Module({
  imports: [
    DatabaseModule
  ],
  controllers: [
    CronController
  ],
  components: [
    ...cronProviders,
    CronService
  ]
})
export class ApplicationModule {}
