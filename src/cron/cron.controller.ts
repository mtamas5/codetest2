import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { CronService } from './cron.service';
import { CronKeyGuard } from '../main/guards/cron-key.guard';

@Controller('notification')
export class CronController {
  constructor (
    private readonly cronService: CronService
  ) {}

  @Post('standup-created')
  @UseGuards(CronKeyGuard)
  public async handleStandupCreated (@Body() body: any) {
    await this.cronService.scheduleTask({
      id: body.id,
      time: body.time
    });
  }
}
