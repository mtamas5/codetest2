import * as dotenv from 'dotenv';

dotenv.config();

const getEnvKey = (key: string): string => {
  if (process.env[key] === undefined) {
    throw new Error(`Property "${key}" is missing in environment.`);
  }

  return process.env[key] as string;
};

// App
export const NODE_ENV = getEnvKey('NODE_ENV');
export const APP_PORT = Number(getEnvKey('APP_PORT'));
export const APP_BASE_URL = getEnvKey('APP_BASE_URL');
export const CRON_APP_PORT = Number(getEnvKey('CRON_APP_PORT'));
export const CRON_APP_URL = getEnvKey('CRON_APP_URL');
export const CRON_APP_KEY = getEnvKey('CRON_APP_KEY');

// Database
export const DB_HOST = getEnvKey('DB_HOST');
export const DB_NAME = getEnvKey('DB_NAME');
export const DB_PORT = Number(getEnvKey('DB_PORT'));
export const DB_USERNAME = getEnvKey('DB_USERNAME');
export const DB_PASSWORD = getEnvKey('DB_PASSWORD');

// Slack
export const SLACK_VERIFICATION_TOKEN = getEnvKey('SLACK_VERIFICATION_TOKEN');
export const SLACK_CLIENT_ID = getEnvKey('SLACK_CLIENT_ID');
export const SLACK_CLIENT_SECRET = getEnvKey('SLACK_CLIENT_SECRET');
