import { Module } from '@nestjs/common';
import { WorkspaceModule } from './modules/workspace/workspace.module';
import { EventListenerModule } from './modules/event-listener/event-listener.module';
import { CommandModule } from './modules/command/command.module';
import { InteractiveModule } from './modules/interactive/interactive.module';
import { GlobalModule } from './modules/global/global.module';

@Module({
  imports: [
    GlobalModule,
    WorkspaceModule,
    EventListenerModule,
    CommandModule,
    InteractiveModule
  ]
})
export class ApplicationModule {}
