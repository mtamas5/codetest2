import { WorkspaceEntity } from '../../workspace/workspace.entity';

export interface IInitializePersonalStandupPayload {
  workspace: WorkspaceEntity;
  user_id: string;
}
