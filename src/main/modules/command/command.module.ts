import { Module } from '@nestjs/common';
import { ConversationModule } from '../conversation/conversation.module';
import { CommandController } from './command.controller';
import { WorkspaceModule } from '../workspace/workspace.module';
import { FeatureModule } from '../feature-config/feature-config.module';
import { CommandService } from './command.service';
import { DatabaseModule } from '../database/database.module';
import { StandUpModule } from '../stand-up/stand-up.module';

@Module({
  imports: [
    DatabaseModule,
    WorkspaceModule,
    FeatureModule,
    ConversationModule,
    StandUpModule
  ],
  controllers: [
    CommandController
  ],
  components: [
    CommandService
  ]
})
export class CommandModule {}
