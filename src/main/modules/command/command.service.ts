import { Component, Inject } from '@nestjs/common';
import { STAND_UP_FEATURE_TYPE } from '../feature-config/types';
import * as moment from 'moment';
import { FeatureConfigService } from '../feature-config/feature-config.service';
import { StandUpHandler } from '../conversation/handlers';
import { IInitializePersonalStandupPayload } from './interfaces';
import { FeatureNotFullyConfiguredException } from '../conversation/exception';
import { SLACK_WEB_CLIENT_TOKEN, DB_CONNECTION_TOKEN } from '../../constants';
import { WebClient } from '@slack/client';
import { Connection } from 'typeorm';
import { FeatureConfigEntity } from '../feature-config/feature-config.entity';
import { StandUpService } from '../stand-up/stand-up.service';

@Component()
export class CommandService {
  constructor (
    @Inject(SLACK_WEB_CLIENT_TOKEN)
    private readonly slackWebClient: WebClient,
    @Inject(DB_CONNECTION_TOKEN)
    private readonly dbConnection: Connection,
    private readonly featureConfigService: FeatureConfigService,
    private readonly standUpHandler: StandUpHandler,
    private readonly standUpService: StandUpService
  ) {}

  public async initializePersonalStandUp (
    payload: IInitializePersonalStandupPayload
  ) {
    this.standUpHandler.initializeConversation({
      workspace: payload.workspace,
      user_id: payload.user_id,
      datetime: moment.utc().format()
    });
  }

  public async initializeGroupalStandup (id: string, datetime: string) {
    const feature_config = await this.featureConfigService.findOneFeatureConfig({
      relations: ['original_conversation', 'workspace'],
      where: {
        id,
        feature_name: STAND_UP_FEATURE_TYPE
      }
    });

    if (!feature_config.original_conversation.is_finished) {
      throw new FeatureNotFullyConfiguredException();
    }

    const standUpUsers = await this.featureConfigService.findOneFeatureConfigValue({
      where: {
        feature_config,
        configuration_key: 'users'
      }
    });

    standUpUsers.configuration_value.split(' ').forEach((user_id) => {
      this.standUpHandler.initializeConversation({
        workspace: feature_config.workspace,
        user_id,
        feature_config,
        datetime
      });
    });
  }

  public async notifyMissingUsers (feature_config_id: string, stand_ups: any[]) {
    if (!stand_ups.length) {
      throw new Error('Stand ups array must have at least one element.');
    }

    const { channel_id, bot_access_token } = await this.dbConnection
      .createQueryBuilder(FeatureConfigEntity, 'fc')
      .select('fcv.configuration_value channel_id, w.bot_access_token')
      .innerJoin('fc.feature_config_values', 'fcv', 'fc.id = fcv.featureConfigId and fcv.configuration_key = "channel"')
      .innerJoin('fc.workspace', 'w')
      .where('fc.id = :feature_config_id', { feature_config_id })
      .getRawOne();

    const users = stand_ups
      .map(s => `<@${s.user_id}>`)
      .join(', ')
      .replace(/,(?=[^,]*$)/, ' and');

    await this.slackWebClient.chat.postMessage({
      token: bot_access_token,
      channel: channel_id,
      text: `It seems like ${users} ${users.length > 1 ? 'haven\'t' : 'hasn\'t'} finished today's standup.`
    });

    for (const { id } of stand_ups) {
      await this.standUpService.setWasNotified(id, true);
    }
  }
}
