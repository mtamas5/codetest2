import { Controller, Post, Body, Inject, UseGuards } from '@nestjs/common';
import { WorkspaceService } from '../workspace/workspace.service';
import { ProjectConfigHandler, StandUpConfigHandler } from '../conversation/handlers';
import { SLACK_WEB_CLIENT_TOKEN } from '../../constants';
import { WebClient } from '@slack/client';
import { CommandService } from './command.service';
import { UnknownFeatureConfigException } from '../feature-config/exceptions';
import { FeatureNotFullyConfiguredException } from '../conversation/exception';
import { SlackKeyGuard, CronKeyGuard } from '../../guards';

@Controller('commands')
export class CommandController {
  constructor (
    @Inject(SLACK_WEB_CLIENT_TOKEN)
    private readonly slackWebClient: WebClient,
    private readonly workspaceService: WorkspaceService,
    private readonly standUpConfigHandler: StandUpConfigHandler,
    private readonly projectConfigHandler: ProjectConfigHandler,
    private readonly commandService: CommandService
  ) {}

  @Post('standup')
  @UseGuards(SlackKeyGuard)
  public async handleStandupCommand (@Body() body: any) {
    const workspace = await this.workspaceService.findByTeamId(body.team_id);

    try {
      return await this.commandService.initializePersonalStandUp({
        workspace,
        user_id: body.user_id
      });
    } catch (e) {
      if (e instanceof UnknownFeatureConfigException) {
        await this.slackWebClient.chat.postEphemeral({
          token: workspace.bot_access_token,
          channel: body.channel_id,
          user: body.user_id,
          text: 'It seems like you don\'t have any stand ups configured yet. ' +
          'Please configure one and try again.'
        });
      } else
      if (e instanceof FeatureNotFullyConfiguredException) {
        await this.slackWebClient.chat.postEphemeral({
          token: workspace.bot_access_token,
          channel: body.channel_id,
          user: body.user_id,
          text: 'It seems like this stand up is not fully configured yet.' +
          'Please finish the configuration and try again.'
        });
      } else {
        throw e;
      }
    }
  }

  @Post('standupconfig')
  @UseGuards(SlackKeyGuard)
  public async handleStandupConfig (@Body() body: any) {
    this.standUpConfigHandler.initializeConversation({
      workspace: await this.workspaceService.findByTeamId(body.team_id),
      created_by: body.user_id,
      user_id: body.user_id
    });
  }

  @UseGuards(SlackKeyGuard)
  @Post('projectconfig')
  public async handleProjectConfig (@Body() body: any) {
    this.projectConfigHandler.initializeConversation({
      workspace: await this.workspaceService.findByTeamId(body.team_id),
      created_by: body.user_id,
      user_id: body.user_id
    });
  }

  @Post('cron/groupalstandup')
  @UseGuards(CronKeyGuard)
  public async handleGroupalStandup (@Body() body: any) {
    return await this.commandService.initializeGroupalStandup(body.id, body.datetime);
  }

  @Post('cron/notify-missing-users')
  @UseGuards(CronKeyGuard)
  public async notifyMissingUsers (@Body() body: any) {
    return await this.commandService.notifyMissingUsers(body.feature_config_id, body.stand_ups);
  }
}
