import { Component, Inject } from '@nestjs/common';
import { WorkspaceService } from '../workspace/workspace.service';
import { WebClient } from '@slack/client';
import { ConversationService } from '../conversation/conversation.service';
import {
  STAND_UP_CONFIG_CONVERSATION_TYPE,
  PROJECT_CONFIG_CONVERSATION_TYPE,
  STAND_UP_CONVERSATION_TYPE
} from '../conversation/types';
import { UnknownConversationTypeException } from '../conversation/exception';
import { ConversationEntity } from '../conversation/conversation.entity';
import { SLACK_WEB_CLIENT_TOKEN, DB_CONNECTION_TOKEN } from '../../constants';
import { extractBoolean } from '../../utilities/extractors';
import { ProjectConfigHandler, StandUpConfigHandler, StandUpHandler } from '../conversation/handlers';
import { IConversationHandler } from '../conversation/interfaces';
import { ConversationTypeEntity } from '../conversation/conversation-type.entity';
import { Connection } from 'typeorm';

@Component()
export class EventListenerService {
  constructor (
    private readonly workspaceService: WorkspaceService,
    private readonly conversationService: ConversationService,
    private readonly standUpConfigHandler: StandUpConfigHandler,
    private readonly projectConfigHandler: ProjectConfigHandler,
    private readonly standUpHandler: StandUpHandler,
    @Inject(SLACK_WEB_CLIENT_TOKEN)
    private readonly slackWebClient: WebClient,
    @Inject(DB_CONNECTION_TOKEN)
    private readonly dbConnection: Connection
  ) {}

  public async handleEventCallback (body: any) {
    if (body.event.type === 'app_uninstalled') {
      this.workspaceService.deleteWorkspace(body.team_id);
      return;
    }

    if (body.event.type !== 'message' || ['bot_message', 'bot_add'].includes(body.event.subtype)) {
      return;
    }

    if (!body.event.thread_ts) {
      return;
    }

    const conversation = await this.dbConnection
      .getRepository(ConversationEntity)
      .findOne({
        relations: [
          'conversation_type',
          'current_question',
          'workspace',
          'feature_config',
          'original_feature_config',
          'stand_up'
        ],
        where: {
          thread_id: body.event.thread_ts,
          is_finished: false
        }
      });

    if (!conversation || conversation.is_finished) {
      return;
    }

    if (conversation.current_question.configuration_key === '__restart') {
      this.handleRestartMessage(conversation, body);
    } else
    if (conversation.current_question.configuration_key.startsWith('__projectselect')) {
      this.handleProjectConfig(conversation, body);
    } else {
      this.handleConversationMessage(conversation, body);
    }
  }

  private async handleRestartMessage (
    conversation: ConversationEntity,
    body: any
  ) {
    const parsedAnswer = extractBoolean(body.event.text);

    const handler = this.findConversationHandler(conversation.conversation_type);

    if (parsedAnswer === undefined) {
      handler.sendErrorMessage(conversation, body);
      return;
    }

    // Finish current conversation no matter if we should restart the process or not.
    // If we do restart, we will create a brand new conversation row in database.
    await this.conversationService.updateConversationById(conversation.id, {
      is_finished: true
    });

    if (parsedAnswer === '0') {
      handler.handleConversationEnd(conversation, body);
    } else {
      // NOTE: This won't work for stand ups. In that case,
      // we need to pass the body so it can gather the channel id,
      // and a feature_config (which can be taken from current conversation).
      // If stand up restarts are needed in the future, please perform those changes.
      handler.initializeConversation({
        workspace: conversation.workspace,
        thread_id: conversation.thread_id,
        created_by: body.event.user,
        user_id: body.event.user
      });
    }
  }

  private async handleProjectConfig (
    conversation: ConversationEntity,
    body: any
  ) {
    // __projectconfig is meant to be handled by the interactive module,
    // so we just show an error message.
    await this.slackWebClient.chat.postEphemeral({
      token: conversation.workspace.bot_access_token,
      channel: body.event.channel,
      text: 'Please select a project before we continue',
      user: body.event.user,
      thread_ts: body.event.thread_ts
    });
  }

  private async handleConversationMessage (
    conversation: ConversationEntity,
    body: any
  ) {
    const handler = this.findConversationHandler(conversation.conversation_type);

    const parsedAnswer = handler.parseAnswer(conversation, body.event.text);

    if (parsedAnswer === undefined) {
      handler.sendErrorMessage(conversation, body);
      return;
    }

    await handler.saveAnswer(conversation, parsedAnswer);

    if (!(await handler.setNextQuestion(conversation, body))) {
      handler.handleConversationEnd(conversation, body);
    }
  }

  private findConversationHandler (conversation_type: ConversationTypeEntity): IConversationHandler {
    switch (conversation_type.type) {
      case STAND_UP_CONFIG_CONVERSATION_TYPE:
        return this.standUpConfigHandler;

      case PROJECT_CONFIG_CONVERSATION_TYPE:
        return this.projectConfigHandler;

      case STAND_UP_CONVERSATION_TYPE:
        return this.standUpHandler;

      default:
        throw new UnknownConversationTypeException(conversation_type.type);
    }
  }
}
