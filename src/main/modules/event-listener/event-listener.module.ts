import { Module } from '@nestjs/common';
import { WorkspaceModule } from '../workspace/workspace.module';
import { EventListenerController } from './event-listener.controller';
import { EventListenerService } from './event-listener.service';
import { ConversationModule } from '../conversation/conversation.module';
import { FeatureModule } from '../feature-config/feature-config.module';
import { StandUpModule } from '../stand-up/stand-up.module';
import { DatabaseModule } from '../database/database.module';

// TODO: Move handlers to conversation module instead of event-listener.

@Module({
  imports: [
    WorkspaceModule,
    ConversationModule,
    FeatureModule,
    StandUpModule,
    DatabaseModule
  ],
  controllers: [
    EventListenerController
  ],
  components: [
    EventListenerService
  ]
})
export class EventListenerModule {}
