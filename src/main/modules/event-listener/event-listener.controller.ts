import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { EventListenerService } from './event-listener.service';
import { SlackKeyGuard } from '../../guards';

@Controller()
export class EventListenerController {
  constructor (
    private readonly eventListenerService: EventListenerService
  ) {}

  @Post('/event-listener')
  @UseGuards(SlackKeyGuard)
  public async listenEvent (@Body() body: any) {
    switch (body.type) {
      case 'url_verification':
        return body.challenge;
      case 'event_callback':
        return this.eventListenerService.handleEventCallback(body);
    }
  }
}
