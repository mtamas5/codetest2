import { ApiModelProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsString } from 'class-validator';

@Exclude()
export class CreateWorkspaceDto {
  @Expose()
  @IsString()
  @ApiModelProperty()
  public readonly code: string;
}
