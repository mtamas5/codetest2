import { WORKSPACE_REPOSITORY_TOKEN, DB_CONNECTION_TOKEN } from '../../constants';
import { Connection } from 'typeorm';
import { WorkspaceEntity } from './workspace.entity';

export const workspaceProviders = [
  {
    provide: WORKSPACE_REPOSITORY_TOKEN,
    useFactory: (connection: Connection) => {
      return connection.getRepository(WorkspaceEntity);
    },
    inject: [DB_CONNECTION_TOKEN]
  }
];
