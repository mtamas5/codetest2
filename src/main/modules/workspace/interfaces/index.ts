export interface ICreateWorkspacePayload {
  team_id: string;
  installer_user_id: string;
  webhook_url: string;
  access_token: string;
  bot_access_token: string;
  bot_user_id: string;
}
