import { Module, forwardRef } from '@nestjs/common';
import { WorkspaceController } from './workspace.controller';
import { workspaceProviders } from './workspace.providers';
import { WorkspaceService } from './workspace.service';
import { DatabaseModule } from '../database/database.module';
import { BotModule } from '../bot/bot.module';

@Module({
  imports: [
    DatabaseModule,
    forwardRef(() => BotModule)
  ],
  controllers: [
    WorkspaceController
  ],
  components: [
    ...workspaceProviders,
    WorkspaceService
  ],
  exports: [
    WorkspaceService
  ]
})
export class WorkspaceModule {}
