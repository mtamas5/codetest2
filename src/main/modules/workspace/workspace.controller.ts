import { Controller, Inject, Response, Get, Query } from '@nestjs/common';
import { CreateWorkspaceDto } from './dto/create-workspace.dto';
import { WebClient } from '@slack/client';
import { SLACK_CLIENT_ID, SLACK_CLIENT_SECRET } from '../../../config';
import { WorkspaceService } from './workspace.service';
import { DuplicateTeamException } from './exceptions';
import { SLACK_WEB_CLIENT_TOKEN } from '../../constants';
import { Response as Res } from 'express';
import { BotService } from '../bot/bot.service';

@Controller()
export class WorkspaceController {
  constructor (
    private readonly workspaceService: WorkspaceService,
    @Inject(SLACK_WEB_CLIENT_TOKEN)
    private readonly slackWebClient: WebClient,
    private readonly botService: BotService
  ) {}

  @Get('/slack-auth')
  public async createWorkspace (
    @Query() { code }: CreateWorkspaceDto,
    @Response() res: Res
  ) {
    // TODO: Check how to handle slack verification token in this endpoint.
    const authResult: any = await this.slackWebClient.oauth.access({
      client_id: SLACK_CLIENT_ID,
      client_secret: SLACK_CLIENT_SECRET,
      code
    });

    try {
      await this.workspaceService.createWorkspace({
        team_id: authResult.team_id,
        installer_user_id: authResult.user_id,
        access_token: authResult.access_token,
        bot_user_id: authResult.bot.bot_user_id,
        bot_access_token: authResult.bot.bot_access_token,
        webhook_url: authResult.incoming_webhook.url
      });

      await this.botService.initializeBot(authResult.team_id, authResult.user_id);

      res.status(200).redirect('http://askflo.io/welcome');
    } catch (e) {
      if (e instanceof DuplicateTeamException) {
        res.status(403).json({
          statusCode: 403,
          error: 'Forbidden',
          message: e.message
        });
      } else {
        res.status(500).json({
          statusCode: 500,
          error: 'Internal Server Error'
        });
      }
    }
  }
}
