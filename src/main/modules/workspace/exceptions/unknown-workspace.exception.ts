export class UnknownWorkspaceException extends Error {
  constructor (id: string | number, isByTeam?: boolean) {
    if (isByTeam) {
      super(`Workspace with team id ${id} does not exist.`);
    } else {
      super(`Workspace with id ${id} does not exist.`);
    }
  }
}
