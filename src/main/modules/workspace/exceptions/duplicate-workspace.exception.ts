export class DuplicateTeamException extends Error {
  constructor (id: string) {
    super(`Team with id "${id}" already exists.`);
  }
}
