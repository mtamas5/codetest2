import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { WORKSPACE_ENTITY_NAME } from '../../constants';
import { FeatureConfigEntity } from '../feature-config/feature-config.entity';
import { ConversationEntity } from '../conversation/conversation.entity';
import { StandUpEntity } from '../stand-up/stand-up.entity';

@Entity(WORKSPACE_ENTITY_NAME)
export class WorkspaceEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @OneToMany(() => FeatureConfigEntity, p => p.workspace)
  public feature_configs: FeatureConfigEntity[];

  @OneToMany(() => StandUpEntity, p => p.workspace)
  public stand_ups: StandUpEntity[];

  @OneToMany(() => ConversationEntity, c => c.workspace)
  public conversations: ConversationEntity[];

  @Column({ unique: true })
  public team_id: string;

  @Column()
  public installer_user_id: string;

  @Column()
  public webhook_url: string;

  @Column()
  public access_token: string;

  @Column()
  public bot_access_token: string;

  @Column()
  public bot_user_id: string;
}
