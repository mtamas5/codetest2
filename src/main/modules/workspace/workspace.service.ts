import { Component, Inject } from '@nestjs/common';
import { WORKSPACE_REPOSITORY_TOKEN } from '../../constants';
import { Repository } from 'typeorm';
import { WorkspaceEntity } from './workspace.entity';
import { ICreateWorkspacePayload } from './interfaces';
import { DuplicateTeamException } from './exceptions';
import { UnknownWorkspaceException } from './exceptions/unknown-workspace.exception';

@Component()
export class WorkspaceService {
  constructor (
    @Inject(WORKSPACE_REPOSITORY_TOKEN)
    private readonly workspaceRepository: Repository<WorkspaceEntity>
  ) {}

  public async createWorkspace (payload: ICreateWorkspacePayload): Promise<WorkspaceEntity> {
    const team = await this.workspaceRepository.findOne({
      select: ['id'],
      where: {
        team_id: payload.team_id
      }
    });

    if (team) {
      throw new DuplicateTeamException(payload.team_id);
    }

    return await this.workspaceRepository.save(
      this.workspaceRepository.create(payload)
    );
  }

  public async findById (id: number) {
    const result = await this.workspaceRepository.findOneById(id);

    if (!result) {
      throw new UnknownWorkspaceException(id);
    }

    return result;
  }

  public async findByTeamId (id: string) {
    const result = await this.workspaceRepository.findOne({
      where: {
        team_id: id
      }
    });

    if (!result) {
      throw new UnknownWorkspaceException(id, true);
    }

    return result;
  }

  public deleteWorkspace (team_id: string) {
    return this.workspaceRepository.delete({ team_id });
  }
}
