import { SLACK_WEB_CLIENT_TOKEN, CRON_HTTP_TOKEN } from '../../constants';
import { WebClient } from '@slack/client';
import axios from 'axios';
import { CRON_APP_URL } from '../../../config';

export const globalProviders = [
  {
    provide: SLACK_WEB_CLIENT_TOKEN,
    useValue: new WebClient()
  },
  {
    provide: CRON_HTTP_TOKEN,
    useValue: axios.create({
      baseURL: CRON_APP_URL
    })
  }
];
