import { Module, Global } from '@nestjs/common';
import { globalProviders } from './global.providers';

@Module({
  components: [
    ...globalProviders
  ],
  exports: [
    ...globalProviders
  ]
})
@Global()
export class GlobalModule {}
