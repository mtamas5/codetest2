import { Module } from '@nestjs/common';
import { InteractiveController } from './interactive.controller';
import { ConversationModule } from '../conversation/conversation.module';

@Module({
  imports: [
    ConversationModule
  ],
  controllers: [
    InteractiveController
  ]
})
export class InteractiveModule {}
