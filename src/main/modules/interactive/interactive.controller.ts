import { Controller, Post, Body } from '@nestjs/common';
import { StandUpConfigHandler, StandUpHandler } from '../conversation/handlers';

@Controller()
export class InteractiveController {
  constructor (
    private readonly standupConfigHandler: StandUpConfigHandler,
    private readonly standupHandler: StandUpHandler
  ) {}

  @Post('/interactive')
  public async handleInteractive (@Body() body: any) {
    body.payload = JSON.parse(body.payload);

    switch (body.payload.callback_id) {
      case 'standup_project-select':
        await this.standupHandler.handleProjectSelect(body);
        break;
      case 'standup-config_project-select':
        await this.standupConfigHandler.handleProjectSelect(body);
        break;
    }
  }
}
