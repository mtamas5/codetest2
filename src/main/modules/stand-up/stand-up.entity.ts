import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToOne,
  OneToMany,
  JoinColumn,
  Column
} from 'typeorm';
import { STAND_UP_ENTITY_NAME } from '../../constants';
import { WorkspaceEntity } from '../workspace/workspace.entity';
import { ConversationEntity } from '../conversation/conversation.entity';
import { StandUpDetailEntity } from './stand-up-detail.entity';

@Entity(STAND_UP_ENTITY_NAME)
export class StandUpEntity {
  constructor (payload?: { id?: number }) {
    if (payload && payload.id) {
      this.id = payload.id;
    }
  }

  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => WorkspaceEntity, w => w.stand_ups, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  public workspace: WorkspaceEntity;

  @OneToOne(() => ConversationEntity, c => c.conversation_type, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  public conversation: ConversationEntity;

  @OneToMany(() => StandUpDetailEntity, v => v.stand_up)
  public stand_up_details: StandUpDetailEntity[];

  @Column({ type: 'datetime' })
  public datetime: string;

  @Column()
  public user_id: string;

  @Column({ default: false })
  public was_notified: boolean;
}
