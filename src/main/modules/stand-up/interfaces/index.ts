import { ConversationQuestionEntity } from '../../conversation/conversation-question.entity';
import { StandUpEntity } from '../stand-up.entity';
import { WorkspaceEntity } from '../../workspace/workspace.entity';
import { ConversationEntity } from '../../conversation/conversation.entity';

export interface ICreateStandUpPayload {
  workspace: WorkspaceEntity;
  conversation: ConversationEntity;
  datetime: string;
  user_id: string;
}

export interface ICreateStandUpDetailPayload {
  stand_up: StandUpEntity;
  question: ConversationQuestionEntity;
  answer: string;
}
