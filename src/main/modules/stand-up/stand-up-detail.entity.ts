import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';
import { STAND_UP_DETAIL_ENTITY_NAME } from '../../constants';
import { StandUpEntity } from './stand-up.entity';
import { ConversationQuestionEntity } from '../conversation/conversation-question.entity';

@Entity(STAND_UP_DETAIL_ENTITY_NAME)
export class StandUpDetailEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => StandUpEntity, s => s.stand_up_details, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  public stand_up: StandUpEntity;

  @ManyToOne(() => ConversationQuestionEntity, q => q.stand_up_details, { nullable: false })
  public question: ConversationQuestionEntity;

  @Column()
  public answer: string;
}
