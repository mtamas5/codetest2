import {
  DB_CONNECTION_TOKEN,
  STAND_UP_REPOSITORY_TOKEN,
  STAND_UP_DETAIL_REPOSITORY_TOKEN
} from '../../constants';
import { Connection } from 'typeorm';
import { StandUpEntity } from './stand-up.entity';
import { StandUpDetailEntity } from './stand-up-detail.entity';

export const standUpProviders = [
  {
    provide: STAND_UP_REPOSITORY_TOKEN,
    inject: [DB_CONNECTION_TOKEN],
    useFactory (connection: Connection) {
      return connection.getRepository(StandUpEntity);
    }
  },
  {
    provide: STAND_UP_DETAIL_REPOSITORY_TOKEN,
    inject: [DB_CONNECTION_TOKEN],
    useFactory (connection: Connection) {
      return connection.getRepository(StandUpDetailEntity);
    }
  }
];
