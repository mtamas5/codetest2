import { Component, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { STAND_UP_REPOSITORY_TOKEN, STAND_UP_DETAIL_REPOSITORY_TOKEN } from '../../constants';
import { StandUpEntity } from './stand-up.entity';
import { StandUpDetailEntity } from './stand-up-detail.entity';
import { ICreateStandUpPayload, ICreateStandUpDetailPayload } from './interfaces';

@Component()
export class StandUpService {
  constructor (
    @Inject(STAND_UP_REPOSITORY_TOKEN)
    private readonly standUpRepository: Repository<StandUpEntity>,
    @Inject(STAND_UP_DETAIL_REPOSITORY_TOKEN)
    private readonly standUpDetailRepository: Repository<StandUpDetailEntity>
  ) {}

  public createStandUp (payload: ICreateStandUpPayload) {
    return this.standUpRepository.save(
      this.standUpRepository.create({
        workspace: payload.workspace,
        conversation: payload.conversation,
        datetime: payload.datetime,
        user_id: payload.user_id
      })
    );
  }

  public createStandUpDetail (payload: ICreateStandUpDetailPayload) {
    return this.standUpDetailRepository.save(
      this.standUpDetailRepository.create(payload)
    );
  }

  public setWasNotified (id: number, value: boolean) {
    return this.standUpRepository
      .createQueryBuilder()
      .update()
      .set({ was_notified: value })
      .where('id = :id', { id })
      .execute();
  }
}
