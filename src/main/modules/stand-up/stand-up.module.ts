import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { standUpProviders } from './stand-up.providers';
import { StandUpService } from './stand-up.service';

@Module({
  imports: [
    DatabaseModule
  ],
  components: [
    ...standUpProviders,
    StandUpService
  ],
  exports: [
    StandUpService
  ]
})
export class StandUpModule {}
