import { Component, Inject } from '@nestjs/common';
import { Repository, FindOneOptions } from 'typeorm';
import { FEATURE_CONFIG_REPOSITORY_TOKEN, FEATURE_CONFIG_VALUE_REPOSITORY_TOKEN } from '../../constants';
import { FeatureConfigEntity } from './feature-config.entity';
import { FeatureConfigValueEntity } from './feature-config-value.entity';
import { WorkspaceEntity } from '../workspace/workspace.entity';
import { ICreateFeaturePayload, ICreateFeatureConfigValuePayload } from './interfaces';
import { FeatureType } from './types';
import { UnknownFeatureConfigException } from './exceptions';
import { UnknownFeatureConfigValueException } from './exceptions/unknown-feature-config-value.exception';

@Component()
export class FeatureConfigService {
  constructor (
    @Inject(FEATURE_CONFIG_REPOSITORY_TOKEN)
    private readonly featureConfigRepository: Repository<FeatureConfigEntity>,
    @Inject(FEATURE_CONFIG_VALUE_REPOSITORY_TOKEN)
    private readonly featureConfigValueRepository: Repository<FeatureConfigValueEntity>
  ) {}

  public createFeatureConfig (payload: ICreateFeaturePayload) {
    return this.featureConfigRepository.save(
      this.featureConfigRepository.create({
        workspace: payload.workspace,
        feature_name: payload.feature_name,
        created_by: payload.created_by,
        original_conversation: payload.original_conversation
      })
    );
  }

  public createFeatureConfigValue (payload: ICreateFeatureConfigValuePayload) {
    return this.featureConfigValueRepository.save(
      this.featureConfigValueRepository.create(payload)
    );
  }

  public async isExistingFeature (
    workspace: WorkspaceEntity,
    feature_name: FeatureType
  ): Promise<boolean> {
    return !!(await this.featureConfigRepository.findOne({
      where: {
        workspace,
        feature_name
      }
    }));
  }

  public async findOneFeatureConfig (
    options: FindOneOptions<FeatureConfigEntity>
  ): Promise<FeatureConfigEntity> {
    const result = await this.featureConfigRepository.findOne(options);

    if (!result) {
      throw new UnknownFeatureConfigException();
    }

    return result;
  }

  public async findOneFeatureConfigValue (
    options: FindOneOptions<FeatureConfigValueEntity>
  ): Promise<FeatureConfigValueEntity> {
    const result = await this.featureConfigValueRepository.findOne(options);

    if (!result) {
      throw new UnknownFeatureConfigValueException();
    }

    return result;
  }
}
