type StandUpFeatureType = 'stand-up';
type ProjectFeatureType = 'project';

export const STAND_UP_FEATURE_TYPE: StandUpFeatureType = 'stand-up';
export const PROJECT_FEATURE_TYPE: ProjectFeatureType = 'project';

export type FeatureType = StandUpFeatureType | ProjectFeatureType;

export const FeatureTypeArr: FeatureType[] = [
  STAND_UP_FEATURE_TYPE,
  PROJECT_FEATURE_TYPE
];
