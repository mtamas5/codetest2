import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, OneToOne, JoinColumn } from 'typeorm';
import { FEATURE_CONFIG_ENTITY_NAME } from '../../constants';
import { WorkspaceEntity } from '../workspace/workspace.entity';
import { FeatureConfigValueEntity } from './feature-config-value.entity';
import { ConversationEntity } from '../conversation/conversation.entity';
import { FeatureType } from './types';

@Entity(FEATURE_CONFIG_ENTITY_NAME)
export class FeatureConfigEntity {
  constructor (payload?: { id?: number }) {
    if (payload && payload.id) {
      this.id = payload.id;
    }
  }

  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => WorkspaceEntity, w => w.feature_configs, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  public workspace: WorkspaceEntity;

  // original_conversation is the conversation we used to configure a particular
  // feature_configuration.
  @OneToOne(() => ConversationEntity, c => c.original_feature_config, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  public original_conversation: ConversationEntity;

  // conversations are the conversations using this feature configuration,
  // for instance, we can have 10 stand ups using the same configuration.
  @OneToMany(() => ConversationEntity, c => c.feature_config)
  public conversations: ConversationEntity[];

  @OneToMany(() => FeatureConfigValueEntity, v => v.feature_config)
  public feature_config_values: FeatureConfigValueEntity[];

  @Column({ type: 'varchar' })
  public feature_name: FeatureType;

  @Column()
  public created_by: string;
}
