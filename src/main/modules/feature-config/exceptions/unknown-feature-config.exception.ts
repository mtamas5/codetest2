export class UnknownFeatureConfigException extends Error {
  constructor () {
    super(`Unknown feature config.`);
  }
}
