export class UnknownFeatureConfigValueException extends Error {
  constructor () {
    super(`Unknown feature config value.`);
  }
}
