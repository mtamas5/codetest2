import {
  DB_CONNECTION_TOKEN,
  FEATURE_CONFIG_REPOSITORY_TOKEN,
  FEATURE_CONFIG_VALUE_REPOSITORY_TOKEN
} from '../../constants';
import { Connection } from 'typeorm';
import { FeatureConfigEntity } from './feature-config.entity';
import { FeatureConfigValueEntity } from './feature-config-value.entity';

export const featureProviders = [
  {
    provide: FEATURE_CONFIG_REPOSITORY_TOKEN,
    inject: [DB_CONNECTION_TOKEN],
    useFactory (connection: Connection)  {
      return connection.getRepository(FeatureConfigEntity);
    }
  },
  {
    provide: FEATURE_CONFIG_VALUE_REPOSITORY_TOKEN,
    inject: [DB_CONNECTION_TOKEN],
    useFactory (connection: Connection) {
      return connection.getRepository(FeatureConfigValueEntity);
    }
  }
];
