import { FeatureConfigEntity } from '../feature-config.entity';
import { WorkspaceEntity } from '../../workspace/workspace.entity';
import { FeatureType } from '../types';
import { ConversationEntity } from '../../conversation/conversation.entity';

export interface ICreateFeaturePayload {
  workspace: WorkspaceEntity;
  feature_name: FeatureType;
  created_by: string;
  original_conversation: ConversationEntity;
}

export interface ICreateFeatureConfigValuePayload {
  feature_config: FeatureConfigEntity;
  configuration_key: string;
  configuration_value: string;
}
