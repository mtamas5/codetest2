import { Module } from '@nestjs/common';
import { featureProviders } from './feature-config.providers';
import { DatabaseModule } from '../database/database.module';
import { FeatureConfigService } from './feature-config.service';

@Module({
  imports: [
    DatabaseModule
  ],
  components: [
    ...featureProviders,
    FeatureConfigService
  ],
  exports: [
    FeatureConfigService
  ]
})
export class FeatureModule {}
