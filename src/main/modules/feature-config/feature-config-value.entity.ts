import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';
import { FEATURE_CONFIG_VALUE_ENTITY_NAME } from '../../constants';
import { FeatureConfigEntity } from './feature-config.entity';

@Entity(FEATURE_CONFIG_VALUE_ENTITY_NAME)
export class FeatureConfigValueEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => FeatureConfigEntity, v => v.feature_config_values, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  public feature_config: FeatureConfigEntity;

  @Column()
  public configuration_key: string;

  @Column()
  public configuration_value: string;
}
