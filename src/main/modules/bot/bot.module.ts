import { Module, forwardRef } from '@nestjs/common';
import { WorkspaceModule } from '../workspace/workspace.module';
import { ConversationModule } from '../conversation/conversation.module';
import { BotService } from './bot.service';

@Module({
  imports: [
    forwardRef(() => WorkspaceModule),
    ConversationModule
  ],
  components: [
    BotService
  ],
  exports: [
    BotService
  ]
})
export class BotModule {}
