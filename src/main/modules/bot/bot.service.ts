import { Component, Inject } from '@nestjs/common';
import { UnknownWorkspaceException } from '../workspace/exceptions';
import { greetingMessage } from '../../messages';
import { WorkspaceService } from '../workspace/workspace.service';
import { SLACK_WEB_CLIENT_TOKEN } from '../../constants';
import { WebClient } from '@slack/client';
import { ProjectConfigHandler } from '../conversation/handlers/project-config';

@Component()
export class BotService {
  constructor (
    @Inject(SLACK_WEB_CLIENT_TOKEN)
    private readonly slackWebClient: WebClient,
    private readonly workspaceService: WorkspaceService,
    private readonly projectConfigHandler: ProjectConfigHandler
  ) {}

  public async initializeBot (team_id: string, user_id: string): Promise<void> {
    // Some times we will receive a "bot_add" event subtype before receiving
    // the authentication confirmation (and creating a workspace in database).
    // To handle that, this method will try to perform a database check once every
    // second during 2 seconds before failing when a workspace is not found.
    let failCount = 0;

    const initialize = async () => {
      try {
        const workspace = await this.workspaceService.findByTeamId(team_id);

        const imChannel = await this.slackWebClient.im.open({
          token: workspace.bot_access_token,
          user: user_id
        });

        // Send greeting.
        await this.slackWebClient.chat.postMessage({
          token: workspace.bot_access_token,
          channel: (imChannel as any).channel.id,
          text: greetingMessage
        });

        await this.projectConfigHandler.initializeConversation({
          workspace,
          created_by: user_id,
          user_id,
          channel_id: (imChannel as any).channel.id
        });
      } catch (e) {
        if (e instanceof UnknownWorkspaceException && ++failCount < 2) {
          setTimeout(initialize, 1000);
          return;
        }

        throw e;
      }
    };

    initialize();
  }
}
