import {
  CONVERSATION_REPOSITORY_TOKEN,
  CONVERSATION_QUESTION_REPOSITORY_TOKEN,
  CONVERSATION_TYPE_REPOSITORY_TOKEN,
  DB_CONNECTION_TOKEN
} from '../../constants';
import { Connection } from 'typeorm';
import { ConversationEntity } from './conversation.entity';
import { ConversationQuestionEntity } from './conversation-question.entity';
import { ConversationTypeEntity } from './conversation-type.entity';

export const conversationProviders = [
  {
    provide: CONVERSATION_REPOSITORY_TOKEN,
    inject: [DB_CONNECTION_TOKEN],
    useFactory (connection: Connection) {
      return connection.getRepository(ConversationEntity);
    }
  },
  {
    provide: CONVERSATION_QUESTION_REPOSITORY_TOKEN,
    inject: [DB_CONNECTION_TOKEN],
    useFactory (connection: Connection) {
      return connection.getRepository(ConversationQuestionEntity);
    }
  },
  {
    provide: CONVERSATION_TYPE_REPOSITORY_TOKEN,
    inject: [DB_CONNECTION_TOKEN],
    useFactory (connection: Connection) {
      return connection.getRepository(ConversationTypeEntity);
    }
  }
];
