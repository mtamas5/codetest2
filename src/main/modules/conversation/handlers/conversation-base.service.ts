import { Component, Inject } from '@nestjs/common';
import { WebClient } from '@slack/client';
import { FeatureConfigService } from '../../feature-config/feature-config.service';
import { StandUpService } from '../../stand-up/stand-up.service';
import { ConversationService } from '../conversation.service';
import { SLACK_WEB_CLIENT_TOKEN, DB_CONNECTION_TOKEN } from '../../../constants';
import { ICreateConversationPayload, IConversationInitializerOptions, ISetNextQuestionPayload } from '../interfaces';
import { UnknownConversationQuestionException } from '../exception';
import { WorkspaceEntity } from '../../workspace/workspace.entity';
import { Connection } from 'typeorm';
import { FeatureConfigValueEntity } from '../../feature-config/feature-config-value.entity';
import { ConversationTypeEntity } from '../conversation-type.entity';

// This class is meant to be a helper for all other handlers.
// It's not meant to be used by any other section of the program.
@Component()
export class ConfigConversationService {
  constructor (
    private readonly featureConfigService: FeatureConfigService,
    private readonly standUpService: StandUpService,
    private readonly conversationService: ConversationService,
    @Inject(SLACK_WEB_CLIENT_TOKEN)
    private readonly slackWebClient: WebClient,
    @Inject(DB_CONNECTION_TOKEN)
    private readonly dbConnection: Connection
  ) {}

  public async initializeConversation (
    options: IConversationInitializerOptions
  ): Promise<void> {
    const conversation_type = await this.conversationService.findConversationType(
      options.conversation_type
    );

    const channel = await this.getChannel(options);

    // Thread id will be present when initializing a conversation after restart.
    if (!options.thread_id) {
      options.thread_id = await this.initializeThread(options, conversation_type, channel);
    }

    // Find the first question
    const conversation_question = await this.conversationService.findConversationQuestion(
      conversation_type,
      options.type === 'stand-up' && options.feature_config ? 2 : 1
    );

    const messageOptions: any = {
      token: options.workspace.bot_access_token,
      channel,
      thread_ts: options.thread_id
    };

    if (conversation_question.configuration_key.startsWith('__projectselect')) {
      const [, type] = conversation_question.configuration_key.split(':');

      let projects;

      if (type === 'with-standups') {
        projects = await this.getProjectsForSelection(options.workspace, true);
      } else
      if (type === 'without-standups') {
        projects = await this.getProjectsForSelection(options.workspace, false);
      } else {
        throw new Error('Unknown __projectselect type: ' + type);
      }

      if (!projects.length) {
        // NOTE: Right now if the user doesn't have available standups,
        // backend will send a message before sending the error.
        // to address that, we should ensure the process can be finished
        // before sending the first message.
        // Also run everything in a transaction.
        await this.slackWebClient.chat.postEphemeral({
          token: options.workspace.bot_access_token,
          channel,
          thread_ts: options.thread_id,
          text: 'It seems like you don\'t have any available projects. Please configure one and try again.',
          user: options.user_id
        });
      }

      messageOptions.attachments = [
        {
          text: conversation_question.question,
          color: '#3AA3E3',
          callback_id: options.type === 'stand-up' ? 'standup_project-select' : 'standup-config_project-select',
          actions: [
            {
              name: 'projects',
              type: 'select',
              options: projects.map(project => ({
                text: project.configuration_value,
                value: project.feature_config_id
              }))
            }
          ]
        }
      ] as any;
    } else {
      messageOptions.text = conversation_question.question;
    }

    // Send first question to user.
    this.slackWebClient.chat.postMessage(messageOptions);

    const conversationPayload: ICreateConversationPayload = {
      workspace: options.workspace,
      conversation_type,
      conversation_question,
      thread_id: options.thread_id as string
    };

    if (options.type === 'stand-up') {
      conversationPayload.feature_config = options.feature_config;
    }

    // Create conversation.
    const conversation = await this.conversationService.createConversation(conversationPayload);

    switch (options.type) {
      case 'feature-config':
        await this.featureConfigService.createFeatureConfig({
          workspace: options.workspace,
          feature_name: options.feature_type,
          created_by: options.created_by,
          original_conversation: conversation
        });
        break;
      case 'stand-up':
        await this.standUpService.createStandUp({
          workspace: options.workspace,
          conversation,
          datetime: options.datetime,
          user_id: options.user_id
        });
        break;
      default:
        throw new Error('options.type must be one of ["feature", "stand-up"]');
    }
  }

  private async getChannel (options: IConversationInitializerOptions): Promise<string> {
    if (options.channel_id) {
      return options.channel_id;
    } else {
      // If we need to send the message to a user, we must first open the channel.
      const imChannel = await this.slackWebClient.im.open({
        token: options.workspace.bot_access_token,
        user: options.user_id
      });

      return (imChannel as any).channel.id;
    }
  }

  private async initializeThread (
    options: IConversationInitializerOptions,
    conversation_type: ConversationTypeEntity,
    channel: string
  ): Promise<string> {
    // Find initial question (which is actually a greeting, not a question).
    const initial_question = await this.conversationService.findConversationQuestion(
      conversation_type, 0
    );

    // Send initial question's text.
    const thread = await this.slackWebClient.chat.postMessage({
      token: options.workspace.bot_access_token,
      channel,
      text: initial_question.question
    });

    return (thread as any).ts;
  }

  /**
   * @description this method will return true if next question was set successfully,
   * and false if conversation type doesn't have more questions. In this case, it
   * will also set the conversation to "finished" state.
   */
  public async setNextQuestion ({ conversation, channel_id }: ISetNextQuestionPayload): Promise<boolean> {
    try {
      // Search the next question.
      const question = await this.conversationService.findConversationQuestion(
        conversation.conversation_type,
        conversation.current_question.question_order + 1
      );

      // Bind next question to conversation.
      await this.conversationService.updateConversationById(conversation.id, {
        current_question: question
      });

      // Ask next question to user.
      await this.slackWebClient.chat.postMessage({
        token: conversation.workspace.bot_access_token,
        channel: channel_id,
        text: question.question,
        thread_ts: conversation.thread_id
      });

      return true;
    } catch (e) {
      if (e instanceof UnknownConversationQuestionException) {
        await this.conversationService.updateConversationById(conversation.id, {
          is_finished: true
        });

        return false;
      } else {
        throw e;
      }
    }
  }

  private getProjectsForSelection (workspace: WorkspaceEntity, isWithStandups: boolean): Promise<any[]> {
    // TODO: If isWithStandups is true, search in standups feature_configuration
    // if the users key contains the user id.
    return this.dbConnection
      .createQueryBuilder()
      .select('fcv.configuration_value, fcv.featureConfigId feature_config_id')
      .from(FeatureConfigValueEntity, 'fcv')
      .innerJoin('feature_configuration', 'fc', 'fc.id = fcv.featureConfigId and fc.feature_name = "project"')
      .leftJoin(subQuery => {
        return subQuery
          .select('fc2.id as id')
          .from(FeatureConfigValueEntity, 'fcv2')
          .innerJoin('feature_configuration', 'fc2', 'fc2.id = fcv2.configuration_value')
          .where('fcv2.configuration_key = "project"');
      }, 'inside', 'inside.id = fcv.featureConfigId')
      .where('fc.workspace = :workspace_id', { workspace_id: workspace.id })
      .andWhere('fcv.configuration_key = "name"')
      .andWhere(`inside.id is ${isWithStandups ? 'not' : ''} null`)
      .getRawMany();
  }
}
