import { Component, Inject } from '@nestjs/common';
import { ConfigConversationService } from './conversation-base.service';
import { IConversationHandler, IInitializeConversationOptions } from '../interfaces';
import { STAND_UP_CONVERSATION_TYPE } from '../types';
import { ConversationEntity } from '../conversation.entity';
import { UnknownConfigurationKeyException } from '../exception';
import { StandUpService } from '../../stand-up/stand-up.service';
import { SLACK_WEB_CLIENT_TOKEN, DB_CONNECTION_TOKEN } from '../../../constants';
import { WebClient } from '@slack/client';
import { FeatureConfigService } from '../../feature-config/feature-config.service';
import { Connection } from 'typeorm';
import * as moment from 'moment';
import { StandUpDetailEntity } from '../../stand-up/stand-up-detail.entity';
import { FeatureConfigEntity } from '../../feature-config/feature-config.entity';
import { ConversationService } from '../conversation.service';

@Component()
export class StandUpHandler implements IConversationHandler {
  constructor (
    @Inject(SLACK_WEB_CLIENT_TOKEN)
    private readonly slackWebClient: WebClient,
    @Inject(DB_CONNECTION_TOKEN)
    private readonly dbConnection: Connection,
    private readonly conversationBaseService: ConfigConversationService,
    private readonly standUpService: StandUpService,
    private readonly featureConfigService: FeatureConfigService,
    private readonly conversationService: ConversationService
  ) {}

  public async initializeConversation (options: IInitializeConversationOptions) {
    if (!options.datetime) {
      throw new Error('datetime is required when initializing a stand up conversation.');
    }

    if (!options.user_id) {
      throw new Error('user_id is required when initializing a stand up conversation.');
    }

    return this.conversationBaseService.initializeConversation({
      type: 'stand-up',
      workspace: options.workspace,
      thread_id: options.thread_id,
      conversation_type: STAND_UP_CONVERSATION_TYPE,
      user_id: options.user_id,
      feature_config: options.feature_config,
      datetime: options.datetime
    });
  }

  public parseAnswer (conversation: ConversationEntity, text: string): string | undefined {
    switch (conversation.current_question.configuration_key) {
      case 'yesterday_work':
        return text;
      case 'today_work':
        return text;
      case 'problems':
        return text;
      default:
        throw new UnknownConfigurationKeyException(
          conversation.conversation_type.type,
          conversation.current_question.configuration_key
        );
    }
  }

  public saveAnswer (conversation: ConversationEntity, parsedAnswer: string) {
    return this.standUpService.createStandUpDetail({
      stand_up: conversation.stand_up,
      question: conversation.current_question,
      answer: parsedAnswer
    });
  }

  public setNextQuestion (conversation: ConversationEntity, body: any): Promise<boolean> {
    return this.conversationBaseService.setNextQuestion({
      conversation,
      channel_id: body.event.channel
    });
  }

  public async handleConversationEnd (conversation: ConversationEntity, body: any) {
    this.slackWebClient.chat.postMessage({
      token: conversation.workspace.bot_access_token,
      channel: body.event.channel,
      text: 'Saved!',
      thread_ts: conversation.thread_id
    });

    this.sendStandUpResults(conversation);
  }

  private async sendStandUpResults (conversation: ConversationEntity): Promise<void> {
    if (!conversation.feature_config) {
      throw new Error('Conversation must have a feature_config defined to perform this action.');
    }

    const channel = (await this.featureConfigService.findOneFeatureConfigValue({
      select: ['configuration_value'],
      where: {
        feature_config: conversation.feature_config,
        configuration_key: 'channel'
      }
    })).configuration_value;

    const date = moment.utc(conversation.stand_up.datetime);

    const standUpDetails = (await this.dbConnection
      .createQueryBuilder(StandUpDetailEntity, 'd')
      .innerJoin('d.stand_up', 's')
      .innerJoinAndSelect('d.question', 'q')
      .where('s.user_id = :user_id', { user_id: conversation.stand_up.user_id })
      .andWhere('s.datetime = :datetime', { datetime: date.format('YYYY-MM-DD HH:mm:ss') })
      .getMany())
      .sort((a, b) => {
        if (a.question.question_order < b.question.question_order) {
          return -1;
        } else
        if (a.question.question_order > b.question.question_order) {
          return 1;
        } else {
          return 0;
        }
      }); // Change this to ORDER BY

    this.slackWebClient.chat.postMessage({
      token: conversation.workspace.bot_access_token,
      channel,
      text: `Standup response on ${date.format('MMMM Do')} from <@${conversation.stand_up.user_id}>`,
      attachments: [
        {
          color: '#36a64f',
          fields: standUpDetails.map(item => ({
            title: item.question.question,
            value: item.answer
          })),
          footer: 'Collected by Flo'
        }
      ]
    });
  }

  public sendErrorMessage (conversation: ConversationEntity, body: any) {
    this.slackWebClient.chat.postMessage({
      token: conversation.workspace.bot_access_token,
      channel: body.event.channel,
      text: 'Sorry, i didn\'t understand.',
      thread_ts: conversation.thread_id
    });
  }

  public async handleProjectSelect (body: any) {
    if (body.payload.callback_id !== 'standup_project-select') {
      throw new Error('Invalid callback_id');
    }

    const conversation = await this.dbConnection
      .createQueryBuilder(ConversationEntity, 'c')
      .leftJoinAndSelect('c.conversation_type', 't')
      .leftJoinAndSelect('c.current_question', 'q')
      .leftJoinAndSelect('c.workspace', 'w')
      .leftJoinAndSelect('c.feature_config', 'fc')
      .where('c.thread_id = :thread', { thread: body.payload.original_message.thread_ts })
      .getOne();

    if (!conversation) {
      throw new Error('Invalid conversation.');
    }

    if (conversation.is_finished) {
      this.slackWebClient.chat.postEphemeral({
        token: conversation.workspace.bot_access_token,
        channel: body.payload.channel.id,
        text: 'It seems like this conversation is already finished.',
        user: body.payload.user.id,
        thread_ts: body.payload.original_message.thread_ts
      });
      return;
    }

    if (conversation.feature_config) {
      this.slackWebClient.chat.postEphemeral({
        token: conversation.workspace.bot_access_token,
        channel: body.payload.channel.id,
        text: 'It seems like you have already selected a project for this standup.',
        user: body.payload.user.id,
        thread_ts: body.payload.original_message.thread_ts
      });
      return;
    }

    const candidateProjectId = body.payload.actions[0].selected_options[0].value;

    // feature_config where the project is defined.
    const feature_config = await this.dbConnection
      .createQueryBuilder(FeatureConfigEntity, 'fc')
      .innerJoin('fc.feature_config_values', 'fcv')
      .where('fcv.configuration_key = "project"')
      .andWhere('fcv.configuration_value = :project_id', { project_id: candidateProjectId })
      .getOne();

    if (!feature_config) {
      throw new Error('Invalid feature configuration');
    }

    await this.conversationService.updateConversationById(conversation.id, {
      feature_config
    });

    this.conversationBaseService.setNextQuestion({
      conversation,
      channel_id: body.payload.channel.id
    });
  }
}
