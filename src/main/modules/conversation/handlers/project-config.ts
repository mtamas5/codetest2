import { Component, Inject } from '@nestjs/common';
import { PROJECT_FEATURE_TYPE } from '../../feature-config/types';
import { PROJECT_CONFIG_CONVERSATION_TYPE } from '../types';
import { ConversationEntity } from '../conversation.entity';
import { extractUser } from '../../../utilities/extractors';
import { UnknownConfigurationKeyException } from '../exception';
import { ConfigConversationService } from './conversation-base.service';
import { IConversationHandler, IInitializeConversationOptions } from '../interfaces';
import { StandUpConfigHandler } from './stand-up-config';
import { FeatureConfigService } from '../../feature-config/feature-config.service';
import { STAND_UP_FEATURE_TYPE } from '../../feature-config/types/feature';
import { SLACK_WEB_CLIENT_TOKEN } from '../../../constants';
import { WebClient } from '@slack/client';

@Component()
export class ProjectConfigHandler implements IConversationHandler {
  constructor (
    private readonly conversationBaseService: ConfigConversationService,
    private readonly standUpConfigHandler: StandUpConfigHandler,
    private readonly featureConfigService: FeatureConfigService,
    @Inject(SLACK_WEB_CLIENT_TOKEN)
    private readonly slackWebClient: WebClient
  ) {}

  public initializeConversation (options: IInitializeConversationOptions) {
    if (!options.created_by) {
      throw new Error('options.created_by must be present in options.');
    }

    return this.conversationBaseService.initializeConversation({
      type: 'feature-config',
      workspace: options.workspace,
      thread_id: options.thread_id,
      conversation_type: PROJECT_CONFIG_CONVERSATION_TYPE,
      feature_type: PROJECT_FEATURE_TYPE,
      user_id: options.user_id,
      created_by: options.created_by,
      channel_id: options.channel_id
    });
  }

  public parseAnswer (conversation: ConversationEntity, text: string): string | undefined {
    switch (conversation.current_question.configuration_key) {
      case 'name':
        return text;
      case 'lead':
        return extractUser(text);
      default:
        throw new UnknownConfigurationKeyException(
          conversation.conversation_type.type,
          conversation.current_question.configuration_key
        );
    }
  }

  public saveAnswer (conversation: ConversationEntity, parsedAnswer: string) {
    if (!conversation.original_feature_config) {
      throw new Error('original_feature_config must be specified to perform this action.');
    }

    return this.featureConfigService.createFeatureConfigValue({
      feature_config: conversation.original_feature_config,
      configuration_key: conversation.current_question.configuration_key,
      configuration_value: parsedAnswer
    });
  }

  public setNextQuestion (conversation: ConversationEntity, body: any): Promise<boolean> {
    return this.conversationBaseService.setNextQuestion({
      conversation,
      channel_id: body.event.channel
    });
  }

  public async handleConversationEnd (conversation: ConversationEntity, body: any) {
    // Verify if user has at least one stand up configured.
    const hasStandUpConfigs = await this.featureConfigService.isExistingFeature(
      conversation.workspace,
      STAND_UP_FEATURE_TYPE
    );

    // If he does, send "done" message and exit. Otherwise invite him to
    // configure a stand up.
    if (hasStandUpConfigs) {
      this.slackWebClient.chat.postMessage({
        token: conversation.workspace.bot_access_token,
        channel: body.event.channel,
        text: 'Great! We\'re done for now then.',
        thread_ts: conversation.thread_id
      });
    } else {
      this.slackWebClient.chat.postMessage({
        token: conversation.workspace.bot_access_token,
        channel: body.event.channel,
        text: 'Great! Let\'s configure your first stand up then.',
        thread_ts: conversation.thread_id
      });

      this.standUpConfigHandler.initializeConversation({
        workspace: conversation.workspace,
        created_by: body.event.user,
        user_id: body.event.user,
      });
    }
  }

  public sendErrorMessage (conversation: ConversationEntity, body: any) {
    this.slackWebClient.chat.postMessage({
      token: conversation.workspace.bot_access_token,
      channel: body.event.channel,
      text: 'Sorry, i didn\'t understand.',
      thread_ts: conversation.thread_id
    });
  }
}
