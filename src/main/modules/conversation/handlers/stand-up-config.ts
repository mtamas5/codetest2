import { Component, Inject } from '@nestjs/common';
import { STAND_UP_FEATURE_TYPE, PROJECT_FEATURE_TYPE } from '../../feature-config/types';
import { STAND_UP_CONFIG_CONVERSATION_TYPE } from '../types';
import { ConversationEntity } from '../conversation.entity';
import { UnknownConfigurationKeyException } from '../exception';
import { extractBoolean, extractChannel, extractTime, extractUsersList } from '../../../utilities/extractors';
import { ConfigConversationService } from './conversation-base.service';
import { IConversationHandler, IInitializeConversationOptions } from '../interfaces';
import { SLACK_WEB_CLIENT_TOKEN, DB_CONNECTION_TOKEN, CRON_HTTP_TOKEN } from '../../../constants';
import { FeatureConfigService } from '../../feature-config/feature-config.service';
import { WebClient } from '@slack/client';
import { Connection } from 'typeorm';
import { FeatureConfigEntity } from '../../feature-config/feature-config.entity';
import { FeatureConfigValueEntity } from '../../feature-config/feature-config-value.entity';
import { AxiosStatic } from 'axios';
import { CRON_APP_KEY } from '../../../../config';

@Component()
export class StandUpConfigHandler implements IConversationHandler {
  constructor (
    private readonly conversationBaseService: ConfigConversationService,
    private readonly featureConfigService: FeatureConfigService,
    @Inject(SLACK_WEB_CLIENT_TOKEN)
    private readonly slackWebClient: WebClient,
    @Inject(DB_CONNECTION_TOKEN)
    private readonly dbConnection: Connection,
    @Inject(CRON_HTTP_TOKEN)
    private readonly cronHttp: AxiosStatic
  ) {}

  public initializeConversation (options: IInitializeConversationOptions) {
    if (!options.created_by) {
      throw new Error('options.created_by must be present in options.');
    }

    return this.conversationBaseService.initializeConversation({
      type: 'feature-config',
      workspace: options.workspace,
      thread_id: options.thread_id,
      user_id: options.user_id,
      conversation_type: STAND_UP_CONFIG_CONVERSATION_TYPE,
      feature_type: STAND_UP_FEATURE_TYPE,
      created_by: options.created_by
    });
  }

  public parseAnswer (conversation: ConversationEntity, text: string): string | undefined {
    switch (conversation.current_question.configuration_key) {
      case 'is_auto_start':
        return extractBoolean(text);
      case 'time':
        return extractTime(text);
      case 'channel':
        return extractChannel(text);
      case 'users':
        return extractUsersList(text);
      default:
        throw new UnknownConfigurationKeyException(
          conversation.conversation_type.type,
          conversation.current_question.configuration_key
        );
    }
  }

  public saveAnswer (conversation: ConversationEntity, parsedAnswer: string) {
    if (!conversation.original_feature_config) {
      throw new Error('original_feature_config must be specified to perform this action.');
    }

    return this.featureConfigService.createFeatureConfigValue({
      feature_config: conversation.original_feature_config,
      configuration_key: conversation.current_question.configuration_key,
      configuration_value: parsedAnswer
    });
  }

  public async handleProjectSelect (body: any) {
    if (body.payload.callback_id !== 'standup-config_project-select') {
      throw new Error('Invalid callback_id');
    }

    const conversation = await this.dbConnection
      .createQueryBuilder(ConversationEntity, 'c')
      .leftJoinAndSelect('c.conversation_type', 't')
      .leftJoinAndSelect('c.current_question', 'q')
      .leftJoinAndSelect('c.workspace', 'w')
      .leftJoinAndSelect('c.original_feature_config', 'ofc')
      .leftJoinAndSelect('ofc.feature_config_values', 'v', 'v.configuration_key = "project"')
      .where('c.thread_id = :thread', { thread: body.payload.original_message.thread_ts })
      .getOne();

    if (!conversation) {
      throw new Error('Invalid conversation.');
    }

    if (!conversation.original_feature_config) {
      throw new Error('Conversation must have an original_feature_config.');
    }

    // Check the stand up does't have a project configured yet.
    if (conversation.original_feature_config.feature_config_values.length) {
      this.slackWebClient.chat.postEphemeral({
        token: conversation.workspace.bot_access_token,
        channel: body.payload.channel.id,
        text: 'It seems like this stand up already has a project configured.',
        user: body.payload.user.id,
        thread_ts: body.payload.original_message.thread_ts
      });
      return;
    }

    const candidateProjectId = body.payload.actions[0].selected_options[0].value;

    // Since we don't have a table for projects, we can't use the foreign key mechanism,
    // meaning we need to perform these checks manually.

    // Check the project exists.
    const project = await this.dbConnection
      .createQueryBuilder(FeatureConfigEntity, 'fc')
      .where('id = :id', { id: candidateProjectId })
      .andWhere(`feature_name = "${PROJECT_FEATURE_TYPE}"`)
      .getOne();

    if (!project) {
      this.slackWebClient.chat.postEphemeral({
        token: conversation.workspace.bot_access_token,
        channel: body.payload.channel.id,
        text: 'It seems like this project does not exist.',
        user: body.payload.user.id,
        thread_ts: body.payload.original_message.thread_ts
      });
      return;
    }

    // Check any other stand up don't have this project assigned to it.
    const standUp = await this.dbConnection
      .createQueryBuilder(FeatureConfigValueEntity, 'v')
      .innerJoin('v.feature_config', 'fc')
      .where(`fc.feature_name = "${STAND_UP_FEATURE_TYPE}"`)
      .andWhere('v.configuration_key = "project"')
      .andWhere('v.configuration_value = :project_id', { project_id: candidateProjectId })
      .getOne();

    if (standUp) {
      this.slackWebClient.chat.postEphemeral({
        token: conversation.workspace.bot_access_token,
        channel: body.payload.channel.id,
        text: 'It seems like there is already an stand up configured for this project.',
        user: body.payload.user.id,
        thread_ts: body.payload.original_message.thread_ts
      });
      return;
    }

    // Save the project id into stand up configuration.
    await this.featureConfigService.createFeatureConfigValue({
      feature_config: conversation.original_feature_config,
      configuration_key: 'project',
      configuration_value: candidateProjectId
    });

    // Set the next question.
    this.conversationBaseService.setNextQuestion({
      conversation,
      channel_id: body.payload.channel.id
    });
  }

  public setNextQuestion (conversation: ConversationEntity, body: any): Promise<boolean> {
    return this.conversationBaseService.setNextQuestion({
      conversation,
      channel_id: body.event.channel
    });
  }

  public async handleConversationEnd (conversation: ConversationEntity, body: any) {
    this.slackWebClient.chat.postMessage({
      token: conversation.workspace.bot_access_token,
      channel: body.event.channel,
      text: 'Great!',
      thread_ts: conversation.thread_id
    });

    if (!conversation.original_feature_config) {
      throw new Error('original_feature_config is required to handle stand up config conversation end.');
    }

    const fcv = await this.dbConnection
      .createQueryBuilder(FeatureConfigValueEntity, 'fcv')
      .addFrom(FeatureConfigValueEntity, 'fcv2')
      .where('fcv.feature_config = :fc_id')
      .andWhere('fcv2.feature_config = :fc_id')
      .andWhere('fcv.configuration_key = "time"')
      .andWhere('fcv2.configuration_key = "is_auto_start"')
      .andWhere('fcv2.configuration_value = true')
      .setParameter('fc_id', conversation.original_feature_config.id)
      .getOne();

    if (!fcv) {
      // Asume standup doesn't have is_auto_start set to true. Nothing to do.
      return;
    }

    try {
      await this.cronHttp.post('/notification/standup-created', {
        token: CRON_APP_KEY,
        id: conversation.original_feature_config.id,
        time: fcv.configuration_value
      });
    } catch (e) {
      process.stderr.write(
        'Error on request to cron server after creating ' +
        'stand up with id ' +
        conversation.original_feature_config.id
      );
    }
  }

  public sendErrorMessage (conversation: ConversationEntity, body: any) {
    this.slackWebClient.chat.postMessage({
      token: conversation.workspace.bot_access_token,
      channel: body.event.channel,
      text: 'Sorry, i didn\'t understand.',
      thread_ts: conversation.thread_id
    });
  }
}
