import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { ConversationTypeEntity } from './conversation-type.entity';
import { ConversationEntity } from './conversation.entity';
import { CONVERSATION_QUESTION_ENTITY_NAME } from '../../constants';
import { StandUpDetailEntity } from '../stand-up/stand-up-detail.entity';

@Entity(CONVERSATION_QUESTION_ENTITY_NAME)
export class ConversationQuestionEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => ConversationTypeEntity, c => c.questions, { nullable: false })
  public conversation_type: ConversationTypeEntity;

  @OneToMany(() => ConversationEntity, c => c.current_question)
  public current_questions: ConversationEntity[];

  @OneToMany(() => StandUpDetailEntity, s => s.question)
  public stand_up_details: StandUpDetailEntity[];

  @Column()
  public question: string;

  @Column()
  public configuration_key: string;

  @Column()
  public question_order: number;
}
