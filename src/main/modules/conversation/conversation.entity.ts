import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne } from 'typeorm';
import { CONVERSATION_ENTITY_NAME } from '../../constants';
import { WorkspaceEntity } from '../workspace/workspace.entity';
import { ConversationTypeEntity } from './conversation-type.entity';
import { ConversationQuestionEntity } from './conversation-question.entity';
import { FeatureConfigEntity } from '../feature-config/feature-config.entity';
import { StandUpEntity } from '../stand-up/stand-up.entity';

@Entity(CONVERSATION_ENTITY_NAME)
export class ConversationEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => ConversationTypeEntity, c => c.conversations, { nullable: false })
  public conversation_type: ConversationTypeEntity;

  @ManyToOne(() => WorkspaceEntity, w => w.conversations, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  public workspace: WorkspaceEntity;

  @ManyToOne(() => ConversationQuestionEntity, c => c.current_questions, { nullable: false })
  public current_question: ConversationQuestionEntity;

  // "original_feature_config" is the one used in the conversations when
  // an user is configuring some feature (for instance stand up config).
  @OneToOne(() => FeatureConfigEntity, f => f.original_conversation)
  public original_feature_config?: FeatureConfigEntity;

  // feature_config is a reference to the "original_feature_config" the stand
  // up is using.
  @ManyToOne(() => FeatureConfigEntity, f => f.conversations, {
    onDelete: 'CASCADE'
  })
  public feature_config?: FeatureConfigEntity;

  @OneToOne(() => StandUpEntity, s => s.conversation)
  public stand_up: StandUpEntity;

  @Column()
  public thread_id: string;

  @Column()
  public is_finished: boolean;
}
