import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { CONVERSATION_TYPE_ENTITY_NAME } from '../../constants';
import { ConversationType } from './types';
import { ConversationEntity } from './conversation.entity';
import { ConversationQuestionEntity } from './conversation-question.entity';

@Entity(CONVERSATION_TYPE_ENTITY_NAME)
export class ConversationTypeEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @OneToMany(() => ConversationEntity, c => c.conversation_type)
  public conversations: ConversationEntity[];

  @OneToMany(() => ConversationQuestionEntity, c => c.conversation_type)
  public questions: ConversationQuestionEntity[];

  @Column({ type: 'varchar' })
  public type: ConversationType;
}
