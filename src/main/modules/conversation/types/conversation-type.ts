type StandUpConversationType = 'standup';
type StandUpConfigConversationType = 'standup_config';
type ProjectConfigConversationType = 'project_config';

export const STAND_UP_CONVERSATION_TYPE: StandUpConversationType = 'standup';
export const STAND_UP_CONFIG_CONVERSATION_TYPE: StandUpConfigConversationType = 'standup_config';
export const PROJECT_CONFIG_CONVERSATION_TYPE: ProjectConfigConversationType = 'project_config';

export type ConversationType =
  StandUpConversationType |
  StandUpConfigConversationType |
  ProjectConfigConversationType;

export const ConversationTypeArr: ConversationType[] = [
  STAND_UP_CONVERSATION_TYPE,
  STAND_UP_CONFIG_CONVERSATION_TYPE,
  PROJECT_CONFIG_CONVERSATION_TYPE
];
