export class UnknownConversationTypeException extends Error {
  constructor (type: string) {
    super(`Conversation with type ${type} doesn't exist.`);
  }
}
