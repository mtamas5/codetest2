export class UnknownConversationQuestionException extends Error {
  constructor (type: string, order: number) {
    super(`Conversation of type ${type} does not have a question with order ${order}`);
  }
}
