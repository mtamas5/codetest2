export class UnknownConfigurationKeyException extends Error {
  constructor (conversation_type: string, key: string) {
    super(`Unknown key ${key} in conversation of type ${conversation_type}`);
  }
}
