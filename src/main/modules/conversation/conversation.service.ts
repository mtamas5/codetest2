import { Component, Inject } from '@nestjs/common';
import {
  CONVERSATION_REPOSITORY_TOKEN,
  CONVERSATION_QUESTION_REPOSITORY_TOKEN,
  CONVERSATION_TYPE_REPOSITORY_TOKEN
} from '../../constants';
import { Repository, DeepPartial, FindManyOptions } from 'typeorm';
import { ConversationEntity } from './conversation.entity';
import { ICreateConversationPayload } from './interfaces';
import { ConversationQuestionEntity } from './conversation-question.entity';
import { ConversationTypeEntity } from './conversation-type.entity';
import { UnknownConversationTypeException, UnknownConversationQuestionException } from './exception';
import { ConversationType } from './types';

@Component()
export class ConversationService {
  constructor (
    @Inject(CONVERSATION_REPOSITORY_TOKEN)
    private readonly conversationRepository: Repository<ConversationEntity>,
    @Inject(CONVERSATION_QUESTION_REPOSITORY_TOKEN)
    private readonly conversationQuestionRepository: Repository<ConversationQuestionEntity>,
    @Inject(CONVERSATION_TYPE_REPOSITORY_TOKEN)
    private readonly conversationTypeRepository: Repository<ConversationTypeEntity>
  ) {}

  public countConversations (options: FindManyOptions<ConversationEntity>) {
    return this.conversationRepository.count(options);
  }

  public async createConversation (
    payload: ICreateConversationPayload
  ) {
    return this.conversationRepository.save(
      this.conversationRepository.create({
        workspace: payload.workspace,
        thread_id: payload.thread_id,
        conversation_type: payload.conversation_type,
        current_question: payload.conversation_question,
        is_finished: false,
        feature_config: payload.feature_config
      })
    );
  }

  public async updateConversationById (id: number, payload: DeepPartial<ConversationEntity>) {
    return this.conversationRepository.updateById(id, payload);
  }

  public async findConversationType (label: ConversationType): Promise<ConversationTypeEntity> {
    const conversationType = await this.conversationTypeRepository.findOne({
      where: {
        type: label
      }
    });

    if (!conversationType) {
      throw new UnknownConversationTypeException(label);
    }

    return conversationType;
  }

  public async findConversationQuestion (
    type: ConversationTypeEntity,
    order: number
  ): Promise<ConversationQuestionEntity> {
    const question = await this.conversationQuestionRepository.findOne({
      where: {
        conversation_type: type,
        question_order: order
      }
    });

    if (!question) {
      throw new UnknownConversationQuestionException(type.type, 1);
    }

    return question;
  }

  public createQueryBuilder () {
    return this.conversationRepository.createQueryBuilder();
  }
}
