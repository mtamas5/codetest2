import { WorkspaceEntity } from '../../workspace/workspace.entity';
import { ConversationTypeEntity } from '../conversation-type.entity';
import { ConversationQuestionEntity } from '../conversation-question.entity';
import { FeatureConfigEntity } from '../../feature-config/feature-config.entity';
import { FeatureType } from '../../feature-config/types';
import { ConversationType } from '../../conversation/types';
import { ConversationEntity } from '../../conversation/conversation.entity';

export interface ICreateConversationPayload {
  workspace: WorkspaceEntity;
  thread_id: string;
  conversation_type: ConversationTypeEntity;
  conversation_question: ConversationQuestionEntity;
  feature_config?: FeatureConfigEntity;
}

interface IBaseInitializerOptions {
  workspace: WorkspaceEntity;
  thread_id?: string;
  conversation_type: ConversationType;
  user_id: string;
  channel_id?: string;
}

export interface IStandUpInitializerOptions extends IBaseInitializerOptions {
  type: 'stand-up';
  feature_config?: FeatureConfigEntity;
  datetime: string;
}

export interface IFeatureConfigInitializerOptions extends IBaseInitializerOptions {
  type: 'feature-config';
  feature_type: FeatureType;
  created_by: string;
}

export type IConversationInitializerOptions =
  IStandUpInitializerOptions |
  IFeatureConfigInitializerOptions;

// TODO: Break this interface into pieces so we don't have multiple optional properties.
export interface IInitializeConversationOptions {
  workspace: WorkspaceEntity;
  thread_id?: string;
  user_id: string;
  created_by?: string;
  datetime?: string; // Only for stand_up initializer
  feature_config?: FeatureConfigEntity; // Only for stand_up initializer.
  // channel_id is Useful to not open a second channel in some specific scenarios when we
  // need to open it before starting a conversation, like in bot initialization.
  channel_id?: string;
}

export interface ISetNextQuestionPayload {
  conversation: ConversationEntity;
  channel_id: string;
}

export interface IConversationHandler {
  initializeConversation: (options: IInitializeConversationOptions) => any;
  parseAnswer: (conversation: ConversationEntity, text: string) => string | undefined;
  handleConversationEnd: (conversation: ConversationEntity, body: any) => void;
  saveAnswer: (conversation: ConversationEntity, parsedAnswer: string) => Promise<any>;
  setNextQuestion: (conversation: ConversationEntity, body: any) => Promise<boolean>;
  sendErrorMessage: (conversation: ConversationEntity, body: any) => void;
}
