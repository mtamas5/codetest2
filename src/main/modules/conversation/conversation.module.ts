import { Module } from '@nestjs/common';
import { conversationProviders } from './conversation.providers';
import { ConversationService } from './conversation.service';
import { DatabaseModule } from '../database/database.module';
import { ProjectConfigHandler, StandUpConfigHandler, StandUpHandler, ConfigConversationService } from './handlers';
import { FeatureModule } from '../feature-config/feature-config.module';
import { StandUpModule } from '../stand-up/stand-up.module';

@Module({
  imports: [
    DatabaseModule,
    FeatureModule,
    StandUpModule
  ],
  components: [
    ...conversationProviders,
    ConversationService,
    ProjectConfigHandler,
    StandUpConfigHandler,
    StandUpHandler,
    ConfigConversationService
  ],
  exports: [
    ConversationService,
    ProjectConfigHandler,
    StandUpConfigHandler,
    StandUpHandler
  ]
})
export class ConversationModule {}
