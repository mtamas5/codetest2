import { DB_CONNECTION_TOKEN } from '../../constants';
import { createConnection } from 'typeorm';
import { DB_HOST, DB_PORT, DB_USERNAME, DB_PASSWORD, DB_NAME, NODE_ENV } from '../../../config';

export const databaseProviders = [
  {
    provide: DB_CONNECTION_TOKEN,
    useFactory: async () => {
      try {
        return await createConnection({
          type: 'mysql',
          host: DB_HOST,
          database: DB_NAME,
          port: DB_PORT,
          username: DB_USERNAME,
          password: DB_PASSWORD,
          entities: [
            __dirname + '/../**/*.entity.ts'
          ],
          synchronize: NODE_ENV === 'development',
          logging: ['query', 'error'],
          timezone: 'UTC+0'
        });
      } catch (e) {
        process.stdout.write('Error in database provider: ' + e + '\n');
      }
    }
  }
];
