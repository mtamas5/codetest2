export const greetingMessage =
`Hi, I'm Flo.
Thank you for installing me.
I'm your new AI project management assistant. I'm really looking forward to working with you.
To get started I need to ask you a couple of questions to set up your first project.
Just answer in the threads and we'll begin.`;
