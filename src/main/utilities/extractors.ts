import * as moment from 'moment-timezone';

// All of the config values will be added into a column with "varchar" type.
// That's the reason every method here should return a string.

export const extractBoolean = (text: string): '0' | '1' | undefined => {
  switch (text.trim().toLowerCase()) {
    case 'y':
    case 'yes':
      return '1';
    case 'n':
    case 'no':
      return '0';
  }
};

export const extractTime = (text: string): string | undefined => {
  text = text
    .replace(/\s/g, '') // Remove white spaces.
    .replace(/\./g, ':'); // Replace dots with colons

  const isValid = moment(text, 'HH:mm', true).isValid();

  if (isValid) {
    const [h, m] = text.split(':').map(v => Number(v));
    return moment({ hour: h, minute: m }).tz('Europe/London').utc().format('HH:mm');
  }
};

export const extractChannel = (text: string): string | undefined => {
  // Transform "<#channel_id|channel_name>" to "channel_id|channel_name"
  const regexp = /<#(.+?)>/g;
  const result = regexp.exec(text);

  if (result) {
    // If a channel was extracted, transform "channel_id|channel_name" to just "channel_id"
    return result[1].split('|')[0];
  }
};

export const extractUsersList = (text: string): string | undefined => {
  const regexp = /<@(.+?)>/g;
  const results = [];
  let match;

  do {
    match = regexp.exec(text);
    if (match) {
      results.push(match[1]);
    }
  } while (match);

  if (results.length) {
    return results.join(' ');
  }
};

export const extractUser = (text: string): string | undefined => {
  const regexp = /<@(.+?)>/g;
  const result = regexp.exec(text);

  if (result) {
    return result[1];
  }
};
