import * as helmet from 'helmet';
import * as morgan from 'morgan';
import { APP_PORT } from '../config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ApplicationModule } from './app.module';
import { ValidationPipe } from './pipes';

(async function bootstrap () {
  const app = await NestFactory.create(ApplicationModule, {
    bodyParser: true
  });

  app.use(helmet());
  app.use(morgan('dev'));
  app.useGlobalPipes(new ValidationPipe());

  const swaggerOptions = new DocumentBuilder()
    .setTitle('Flo Slack Bot')
    .setDescription('API documentation')
    .setVersion('1.0.0')
    .setContactEmail('hector.webdeveloper@gmail.com')
    .setSchemes('http')
    .build();

  const document = SwaggerModule.createDocument(app, swaggerOptions);

  SwaggerModule.setup('/api', app, document);

  await app.listen(APP_PORT);

  process.stdout.write(`Application is running in port ${APP_PORT}\n`);
})();
