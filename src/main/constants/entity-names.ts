export const WORKSPACE_ENTITY_NAME = 'workspace';

export const FEATURE_CONFIG_ENTITY_NAME = 'feature_configuration';
export const FEATURE_CONFIG_VALUE_ENTITY_NAME = 'feature_configuration_value';

export const STAND_UP_ENTITY_NAME = 'stand_up';
export const STAND_UP_DETAIL_ENTITY_NAME = 'stand_up_details';

export const CONVERSATION_TYPE_ENTITY_NAME = 'conversation_type';
export const CONVERSATION_ENTITY_NAME = 'conversation';
export const CONVERSATION_QUESTION_ENTITY_NAME = 'conversation_questions';
