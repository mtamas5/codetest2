import { Guard, CanActivate, ExecutionContext } from '@nestjs/common';
import { Request } from 'express';
import { CRON_APP_KEY } from '../../config';

@Guard()
export class CronKeyGuard implements CanActivate {
  public canActivate (request: Request, context: ExecutionContext) {
    return request.body.token === CRON_APP_KEY;
  }
}
