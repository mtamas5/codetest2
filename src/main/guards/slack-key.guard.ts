import { Guard, CanActivate, ExecutionContext } from '@nestjs/common';
import { Request } from 'express';
import { SLACK_VERIFICATION_TOKEN } from '../../config';

@Guard()
export class SlackKeyGuard implements CanActivate {
  public canActivate (request: Request, context: ExecutionContext) {
    return request.body.token === SLACK_VERIFICATION_TOKEN;
  }
}
