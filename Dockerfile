FROM ubuntu:16.04
RUN apt-get update
RUN apt-get install -y curl --allow-unauthenticated
RUN apt-get install -y apt-utils
RUN apt-get install -y build-essential
RUN apt-get install -y npm
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash
RUN apt-get install -y nodejs
RUN npm install -g nodemon
RUN apt-get install -y nginx
