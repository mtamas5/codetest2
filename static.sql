DELETE FROM conversation;
DELETE FROM conversation_questions;
DELETE FROM conversation_type;

INSERT INTO conversation_type(id, type) VALUES (1, 'standup');
INSERT INTO conversation_type(id, type) VALUES (2, 'standup_config');
INSERT INTO conversation_type(id, type) VALUES (3, 'project_config');

-- Since we have to store all the questions in the same table,
-- we have a few "configuration_key"s that are not actually meant to
-- map values to database, but instead they are commands for special behavior.

-- __initial will read the question only when the thread is created. If user decides
-- to restart the process again (using the __restart option), the content of __initial
-- will not be read again. __initial MUST be the first question, and, at the moment,
-- can contain only one message (this may change in the future).

-- __restart will send the question to the user, and read the answer as boolean.
-- If the answer is determined to be true, the conversation will start over
-- from step 0, and if it's false, conversation will be marked as finished (any
-- questions after __restart will not be read).

-- In the future, we should be storing an stringified json object with the message
-- and other related info. That would allow for more complex messages to be
-- stored instead of only text.

INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (1, 0, '__initial', "Standup will start here:");
INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (1, 1, '__projectselect:with-standups', "Which project you wish to start an standup for?");
INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (1, 2, 'yesterday_work', "What were you working on yesterday?");
INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (1, 3, 'today_work', "What are you working on today?");
INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (1, 4, 'problems', "What blocks, problems are you facing with?");

INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (2, 0, '__initial', "Please answer these quick questions in the thread to set up your new stand up.");
INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (2, 1, '__projectselect:without-standups', "Which project you wish to add an standup for?");
INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (2, 2, 'is_auto_start', "Would you like to start your stand ups automatically? Please answer Yes or No.");
INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (2, 3, 'time', "What time would you like your stand ups? Please answer with 24 hour time. eg. 09:00 for 9am, and 14:00 for 2pm");
INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (2, 4, 'channel', "Which channel should i display the result in? eg. #general or general.");
INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (2, 5, 'users', "Please provide the list of users who should be in standups in following format: `@fred @jim @joeharris @chang @boris`");
INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (2, 6, '__restart', "Would you like to add another daily standup configuration?");

INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (3, 0, '__initial', "Please answer this quick questions in the thread to set up your new project.");
INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (3, 1, 'name', "What's the name of your project?");
INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (3, 2, 'lead', "Who is the project lead?");
INSERT INTO conversation_questions
(conversationTypeId, question_order, configuration_key, question)
VALUES (3, 3, '__restart', "Would you like to configure another project?");
