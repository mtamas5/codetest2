#!/bin/bash

docker build -t node_nginx .
docker run \
    -v `pwd`/nginx_staging.conf:/etc/nginx/sites-enabled/nginx.conf \
    -v `pwd`:/app \
    -e NODE_ENV=test \
    -e PORT=8000 \
    -p 443:443 \
    node_nginx /app/setup.local.sh

